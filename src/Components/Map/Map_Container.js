import React, { Component } from 'react';

import DeckGL, {LineLayer, HexagonLayer, MapController} from 'deck.gl';
import MapGL from 'react-map-gl';

import './Map.css';
import MapControl from './Map_Control_Panel';
import res_api from '../../State/API';
export default class Map_Container extends Component {


    constructor() {
        super();
        //initial state
        this.state = {
            points:[],
            viewport: {
                height:window.innerHeight,
                width:window.innerWidth,
                longitude: -71.4266 ,
                latitude: 41.81669,
                zoom: 12,
                pitch: 60,
                bearing: 0
            },
            hasRendered: false
        };
    }
    componentWillMount() {
        // pre-flight processing of data. Ultimately should have a client-side handler with state management.
        let a = new res_api();
        a.getAllPoints().then((res) => {
            let dataPoints = res.data.map(element => {
                if (typeof element.property_location !== "undefined") {
                    return (
                        {
                            COORDINATES: [element.property_location.coordinates[0], element.property_location.coordinates[1]],
                            value: element.total_assessment
                        }
                    )
                }
            });
            this.setState({points: dataPoints});
        });
    }

    componentWillUpdate(nextProps, nextState) {
        // Just here for some debugging
        // console.log("update")
    }
    render() {
        const compart = (newF, oldF) => {
            // This is the result of floating point precision getting wobbly as the map changes, needs to update once and then never again.
            // Custom comparator would be needed to add map interactivity.
            // console.log(newF, oldF);
            if(oldF.length == 0 || newF.length == 0) {
                console.log("Data Vis Update");
                return false;
            } else {
                return true
            }
        };
        const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1Ijoic2NhbGVzNDQiLCJhIjoiY2ppdXB5Z3F0MXRwYzNwbXNkbmJha3hwZSJ9.vme5GIXmYkZY1BFDUXGyRg';
        // console.log(this.state.points);
        const data = this.state.points.filter(entry => typeof entry !== 'undefined');
        const lay = new HexagonLayer({
            id:'hexagon-layer',
            data,
            opacity:0.7,
            colorRange:
                [[242,240,247],
                [218,218,235],
                [88,189,220],
                [158,154,200],
                [117,107,177],
                [84,39,143]],
            pickable: true,
            extruded: true,
            radius: 600,
            elevationRange: [1,500],
            elevationUpperPercentile:97,
            elevationLowerPercentile:0,
            elevationScale: 12,
            dataComparator: compart,
            fp64: true,
            getPosition: d => { if(typeof d !== 'undefined') {
                return d.COORDINATES
            } },
            getElevationValue: points => {
                // Grabbing the median property value.
                let values = points.map( p => {
                    return Number(p.value);
                });
                values.sort();
                let middle = Math.floor(Number(values.length)/2);
                let median = values[middle];
                return median
            },
            onClick: ({object}) => {
                // Showing stats about each hex
                let points = object.points;
                let values = points.map( p => {
                    return Number(p.value);
                });
                values.sort();
                let middle = Math.floor(Number(values.length)/2);
                let median = values[middle];
                alert(`Hex Coords: ${object.centroid.join(', ')}\nMedian Val: ${median} \nTotal Properties in Hex: ${object.points.length}`)
            }
        });
        return (
            <div>
                <MapControl />
                <MapGL
                    {...this.state.viewport}
                    onViewportChange={ (viewport) => this.setState({viewport})}
                    mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN} mapStyle="mapbox://styles/scales44/cjiytcd1e823m2qruw5pfhk94">
                    <DeckGL {...this.state.viewport} layers={[lay]} />
                </MapGL>
            </div>

        );
    }
}