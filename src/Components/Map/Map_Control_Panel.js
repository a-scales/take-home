import React, { Component } from 'react';
import './Map.css';

class App extends Component {
    render() {
        return (
            <div className="Control-Panel">
                <div className="inner-panel">
                    <div className="panel-left">

                    </div>
                    <div className="panel-right">
                        <div className="panel-title" >Density of Propery in Providence</div>
                        <div className="panel-content ">
                            <p>This map is showing the density of properties versus the value of property. Darker color is more dense, higher hex height is more value.</p>
                            <p>This visualization was built using the data provided, along with a few other tools</p>
                            <h2>Deck.dl</h2>
                            <p>Deck.gl was the best tool I could find for this kind of data. with additional GeoJSON data of Providence,
                                there is certainly a better way to display this data- for instance having a zip-code layer to better segment the data. The hexes were just a somewhat arbitrary choice to bin the data into manageable chunks.</p>
                            <h2>MapBox</h2>
                            <p>MapBox was the map layer I used for this project. Google Maps or another mapping API/Library could be used, but
                            OpenStreetMap's data is just as good as the larger players in the market, and the pluggability with Deck.gl and React made this the easy choice.</p>
                            <p>There are more interesting design choices that could have been made- that might not convey much from a data science point of view, but would be interesting to look at.
                                Interactivity added to this project with filtering tools would be compelling. Point cloud data mapped similarly to these hexes, but one to one with each property.</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
