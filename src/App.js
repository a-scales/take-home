import React, { Component } from 'react';

import Map from './Components/Map/Map_Container';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Map></Map>
      </div>
    );
  }
}

export default App;
