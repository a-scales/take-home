import Automerge from 'automerge';


class MSM {

    // i threw this in here, but it wasn't ever used, as I didn't have much time to add interactiv elements.
    // however, this is kinda my alternative to overloading a workflow with flux or redux on a smaller project.
    // it has all the immutability aspects, and the ability to have a timeline of changes, but doesn't have abstractions that make me go insane
    constructor() {
        this.KEY = '_MSM';
        this._S = null;

        // Check if there is an item saved in local storage
        if(typeof localStorage.getItem(this.KEY) !== 'undefined') {
            try {
                // Try to load the document
                this._S = Automerge.load(localStorage.getItem(this.KEY));
            } catch(e) {
                // Catch any error, regenerate a new document
                console.log("State Corrupted/Unreadable... \n Generating new tree");
                this._S = Automerge.init();
            }
        } else {
            // Otherwise, create a new document
            this._S = Automerge.init();
            let serialized_state = Automerge.save(this._S);
            localStorage.setItem(this.KEY, serialized_state);
        }
        // if exists, load it
    }

    /**
     * Save current state to localStorage.
     */
    saveToStorage() {
        let serialized_state = Automerge.save(this._S);
        localStorage.setItem(this.KEY, serialized_state);
    }

    /**
     * Reloads the state from save
     */
    reload() {
        if(typeof localStorage.getItem(this.KEY) !== 'undefined') {
            try {
                // Try to load the document
                var r = Automerge.load(localStorage.getItem(this.KEY));
            } catch(e) {
                // Catch any error, regenerate a new document
                console.log("State Corrupted/Unreadable... \n Saving current state and continuing");
                localStorage.setItem(KEY,Automerge.save(this._S));
                return;
            }
            this._S = r;
        }
    }

    /**
     * Act on the immutable state to commit or get a value.
     * @param action - what action to
     * @param key - key of object in state
     * @param data - object to store
     * @returns {*} -
     */
    act(action, key, data) {
        var actions = ['COMMIT', 'GET']; // TODO: Check for valid actions, break this out into a new module or something like that
        this.reload(); // Reloads the state from local store

        // Action switch
        switch(action) {
            case 'COMMIT':
                let d = JSON.stringify(data);
                let msg = 'Committing new element: '+key;
                this._S = Automerge.change(this._S, msg, doc => {
                    doc[key] = d;
                });
                break;
            case 'GET':
                let d = this._S[key];
                return JSON.parse(d);
                break;
        }

        // Saves the document back to storage
        this.saveToStorage();

    }
}

export default MSM;
