import axios from 'axios';


class res_api {
    constructor() {
        this.url = 'https://data.providenceri.gov/resource/r6n7-qjr6.json'
    }
    // Get All Points

    getAllPoints(){
        return axios.get("https://data.providenceri.gov/resource/r6n7-qjr6.json?$limit=49999&$select=property_location,total_assessment&$where=property_location%20IS%20NOT%20NULL")
    }

    // Get points with $Where
    getPointsWhere(){

    }


}
export default res_api;